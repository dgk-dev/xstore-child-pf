��    *      l  ;   �      �  .   �     �  
   �  
   �     �  	                  $     2     9     ?     O  	   _  	   i     s  
   �     �     �     �     �  
   �     �     �  	   �     �     �               +     :     I     P     ^  Q   m     �     �     �       	          �  )  3   �     	     '	     8	     I	     Q	     ^	     e	     z	     �	     �	     �	     �	     �	     �	     �	  	   �	  	   
     
     ,
     J
     V
     d
     k
     s
     |
     �
  
   �
     �
     �
     �
     �
     �
       A        M     a     u  	   �  	   �  $   �     
   *       #                           	         &                                         $                      "   !                     (                                      )   %            '       A password will be sent to your email address. Add  to cart All Brands All brands Buy now Category: Clear Email Address :  Email address Log in Login Login / Sign in Lost password ? Max price Min price More details My Account My Wishlist No products in the wishlist. No results were found. Password Password * Price Price: Read More Register Related products Remember Me Search Results Search results Select options Share: Shopping Cart Shopping cart  Sorry, no products matched your selection. Please choose a different combination. Username or email address Username or email address * View Wishlist Wishlist Wishlist  Your email address Plural-Forms: nplurals=2; plural=n != 1;
Project-Id-Version: XStore Core
PO-Revision-Date: 2020-05-29 21:09+0000
Last-Translator: 
Language-Team: Español
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-WPHeader: et-core-plugin.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
Report-Msgid-Bugs-To: 
Language: es-ES
X-Loco-Version: 2.3.4; wp-5.4.1 Se enviará la contraseña a tu correo electrónico Añadir al carrito Todas las marcas Todas las marcas Comprar Categoría:  Borrar Correo electrónico: Correo electrónico Entrar Entrar Entrar / Registrarse ¿Olvidaste tu contraseña? Precio máximo Precio mínimo Más detalles Mi cuenta Favoritos No hay productos en favoritos. No se encontraron resultados. Contraseña Contraseña * Precio Precio: Ver más Registrarse Productos relacionados Recordarme Resultados de búsqueda Resultados de búsqueda Seleccionar opciones Compartir:  Carrito Carrito Lo sentimos, no hay resultados que coincidan con la combinación. Correo electrónico Correo electrónico Ver favoritos Favoritos Favoritos Tu dirección de correo electrónico 