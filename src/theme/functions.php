<?php

/** analytics */
// require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** Funciones de chat */
// require(get_stylesheet_directory().'/includes/chat.php');

add_action('wp_enqueue_scripts', 'pf_resources');
function pf_resources(){
    etheme_child_styles();
    wp_register_script('footer-bundle', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
	// wp_localize_script('footer-bundle', 'footerGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
    wp_enqueue_script('footer-bundle');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'pf_theme_lang' );
function pf_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

/**
 * Cambiar mensajes
 */
add_filter( 'gettext', 'pf_change_strings', 20, 3 );
function pf_change_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'There are no shipping options available. Please ensure that your address has been entered correctly, or contact us if you need any help.':
            $translated_text = __( 'No hay métodos de envío para tu zona, por favor verifica que hayas ingresado correctamente tu dirección.', $domain );
        break;
        case 'No shipping options were found for %s.':
            $translated_text = __( 'No hay métodos de envío para %s.', $domain );
        break;
        case 'Shipping to %s.':
            $translated_text = __( 'Enviar a %s.', $domain );
        break;
        case 'Change address':
            $translated_text = __( 'Cambiar dirección', $domain );
        break;
        case 'Guarda mi nombre, correo electrónico y web en este navegador para la próxima vez que comente.':
            $translated_text = __( 'Guarda mi nombre y correo electrónico en este navegador para la próxima vez que comente.', $domain );
        break;
    }
    return $translated_text;
}


//agregar shortcode de logo mobile
add_shortcode( 'pf_logo', 'pf_logo_shortcode' );

function pf_logo_shortcode(){
    global $et_builder_globals;

	$element_options = array();
	// logo img default
	$element_options['logo_img_alt'] = $element_options['sticky_logo_img_alt'] = '';
	$element_options['logo_img'] = ETHEME_BASE_URI.'theme/assets/images/logo.png';

	$element_options['logo_img_et-desktop'] = get_theme_mod( 'logo_img_et-desktop', 'logo' );
	$element_options['logo_img_et-desktop'] = apply_filters('logo_img', $element_options['logo_img_et-desktop']);

	if ( is_array($element_options['logo_img_et-desktop']) ) {
		if ( isset($element_options['logo_img_et-desktop']['id']) && $element_options['logo_img_et-desktop']['id'] != '' ) {
			$element_options['logo_img_alt'] = get_post_meta( $element_options['logo_img_et-desktop']['id'], '_wp_attachment_image_alt', true);
		}
		if ( isset($element_options['logo_img_et-desktop']['url']) && $element_options['logo_img_et-desktop']['url'] != '' ) {
			$element_options['logo_img'] = $element_options['logo_img_et-desktop']['url'];
		}
	}

	$element_options['headers_sticky_logo_img'] = $element_options['logo_img'];
	// retina logo
	$element_options['retina_logo_img_et-desktop'] = get_theme_mod( 'retina_logo_img_et-desktop', '' );
	$element_options['retina_logo_img'] = '';

	// to use desktop styles when use this element in mobile menu for example etc.
    $element_options['etheme_use_desktop_style'] = false;
    $element_options['etheme_use_desktop_style'] = apply_filters( 'etheme_use_desktop_style', $element_options['etheme_use_desktop_style'] );

	$element_options['logo_align'] = 'align-' . get_theme_mod( 'logo_align_et-desktop', 'center' );
	$element_options['logo_align'] .= ( !$element_options['etheme_use_desktop_style'] ) ? ' mob-align-' . get_theme_mod( 'logo_align_et-mobile', 'center' ) : '';

	$element_options['logo_align'] = ' ' . apply_filters('logo_align', $element_options['logo_align']);

	// retina logo sets up
	if ( is_array($element_options['retina_logo_img_et-desktop']) ) {
		if ( isset($element_options['retina_logo_img_et-desktop']['url']) && $element_options['retina_logo_img_et-desktop']['url'] != '' ) {
			$element_options['retina_logo_img'] = $element_options['headers_sticky_retina_logo_img'] = ' srcset="' . $element_options['retina_logo_img_et-desktop']['url'] . ' 2x"';
		}
	}

	// sticky retina none by default

	$element_options['headers_sticky_logo_img_et-desktop'] = get_theme_mod( 'headers_sticky_logo_img_et-desktop', '' );

	// in case sticky logo not set then all logoes will be copied to fixed header too
	if ( is_array($element_options['headers_sticky_logo_img_et-desktop']) ) {
		if (isset($element_options['headers_sticky_logo_img_et-desktop']['url']) && $element_options['headers_sticky_logo_img_et-desktop']['url'] != '') {
			$element_options['headers_sticky_logo_img'] = $element_options['headers_sticky_logo_img_et-desktop']['url'];
		}
		if ( isset($element_options['headers_sticky_logo_img_et-desktop']['id']) && $element_options['headers_sticky_logo_img_et-desktop']['id'] != '' ) {
			$element_options['sticky_logo_img_alt'] = get_post_meta( $element_options['headers_sticky_logo_img_et-desktop']['id'], '_wp_attachment_image_alt', true);
		}
	}

	$element_options['logo_simple_et-desktop'] = true;
	$element_options['logo_simple_et-desktop'] = apply_filters('etheme_logo_simple', $element_options['logo_simple_et-desktop']);

	$element_options['logo_sticky_et-desktop'] = true;
	$element_options['logo_sticky_et-desktop'] = apply_filters('etheme_logo_sticky', $element_options['logo_sticky_et-desktop']);

	$element_options['is_customize_preview'] = apply_filters('is_customize_preview', false);
	$element_options['attributes'] = array();
	if ( $element_options['is_customize_preview'] ) 
		$element_options['attributes'] = array(
			'data-title="' . esc_html__( 'Logo', 'xstore-core' ) . '"',
			'data-element="logo"'
        );
        
    $element_options['logo_img_mobile'] = get_stylesheet_directory_uri().'/img/fenix_logo_black.svg';
    $element_options['logo_img_desktop'] = get_stylesheet_directory_uri().'/img/fenix_logo_white.svg';
?>

<div class="et_element et_b_header-logo<?php echo $element_options['logo_align'] . ( $et_builder_globals['in_mobile_menu'] ? '' : ' et_element-top-level' ); ?>" <?php echo implode( ' ', $element_options['attributes'] ); ?>>
	<a href="<?php echo home_url(); ?>">
		<?php if ( $element_options['logo_simple_et-desktop'] ) : ?>
			<span>
                <img class="logo-desktop" src="<?php echo esc_url($element_options['logo_img_desktop']); ?>" alt="<?php echo $element_options['logo_img_alt']; ?>" <?php echo $element_options['retina_logo_img']; ?>>
                <img class="logo-mobile" src="<?php echo esc_url($element_options['logo_img_mobile']); ?>" alt="<?php echo $element_options['logo_img_alt']; ?>" <?php echo $element_options['retina_logo_img']; ?>>
            </span>
		<?php endif; ?>
		<?php if ( $element_options['logo_sticky_et-desktop'] ) : ?>
			<span class="fixed">
                <img class="logo-desktop" src="<?php echo esc_url($element_options['logo_img_desktop']); ?>" alt="<?php echo $element_options['sticky_logo_img_alt']; ?>">
                <img class="logo-mobile" src="<?php echo esc_url($element_options['logo_img_mobile']); ?>" alt="<?php echo $element_options['sticky_logo_img_alt']; ?>">
            </span>
		<?php endif; ?>
	</a>
</div>

<?php unset($element_options);
}

// Redireccionar valoraciones con errores
add_filter( 'pre_comment_approved', 'pf_comments_error_handler', 500, 2 );
function pf_comments_error_handler($approved, $commentdata){
    if(is_wp_error($approved)){
        $redirect = get_permalink($commentdata['comment_post_ID']).'#comment';
        wc_add_notice($approved->get_error_message(), 'error');
        wp_redirect($redirect);
        exit();
    }
}