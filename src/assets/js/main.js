(function ($) {
    var $invoice = $('#billing_invoice');
    if($invoice.length){
        var $billingFields = $('.woocommerce-billing-fields');
        $invoice.change(function(){
            if(this.checked){
                $billingFields.show();
            }else{
                $billingFields.hide();
            }
        }).change();
    }
})(jQuery);